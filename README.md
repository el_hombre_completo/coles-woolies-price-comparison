# Coles/Woolies price comparison

*Language: Python3*
*Version: 3.8.1*

To find out which supermarket has the cheapest price for our shopping list

Our goal is to have our regular shopping list have conduct a search of coles and
woolworths to find out which supermarket has the cheapest price for each item. 

This is the only way my partner will let me do more coding, if I can produce 
something that she can benefit from.
